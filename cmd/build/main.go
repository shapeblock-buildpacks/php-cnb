package main

import (
	"gitlab.com/shapeblock-buildpacks/php/php"

	"github.com/paketo-buildpacks/packit"
)

func main() {
	packit.Build(php.Build())
}

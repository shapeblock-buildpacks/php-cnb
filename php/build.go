package php

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/paketo-buildpacks/packit"
)

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func Build() packit.BuildFunc {
	return func(context packit.BuildContext) (packit.BuildResult, error) {
		phpVersions := make(map[string]string)
		phpVersions["7.3"] = "https://buildpacks.cloudfoundry.org/dependencies/php/php_7.3.29_linux_x64_cflinuxfs3_1a9bca79.tgz"
		phpVersions["7.4"] = "https://buildpacks.cloudfoundry.org/dependencies/php/php_7.4.21_linux_x64_cflinuxfs3_f519a82f.tgz"
		phpVersions["8.0"] = "https://buildpacks.cloudfoundry.org/dependencies/php/php_8.0.8_linux_x64_cflinuxfs3_33e14443.tgz"

		// fix caching
		entry := context.Plan.Entries[0]
		version, _ := entry.Metadata["version"].(string)
		uri := phpVersions[version]

		// TODO: print helpful message if url isn't present/version not supported.
		phpLayer, err := context.Layers.Get("php")
		if err != nil {
			return packit.BuildResult{}, err
		}
		versionFile := filepath.Join(phpLayer.Path, "version")

		// if version file exists in node layer, return
		if fileExists(versionFile) {
			currentVersion, err := ioutil.ReadFile(versionFile)
			if err != nil {
				return packit.BuildResult{}, err
			}

			if string(currentVersion) == version {
				return packit.BuildResult{
					Plan: context.Plan,
					Layers: []packit.Layer{
						phpLayer,
					},
				}, nil
			}
		}

		fmt.Printf("URI -> %s\n", uri)
		phpLayer, err = phpLayer.Reset()
		if err != nil {
			return packit.BuildResult{}, err
		}

		downloadDir, err := ioutil.TempDir("", "downloadDir")
		if err != nil {
			return packit.BuildResult{}, err
		}
		defer os.RemoveAll(downloadDir)

		fmt.Println("Downloading dependency...")
		err = exec.Command("curl",
			uri,
			"-o", filepath.Join(downloadDir, "php.tgz"),
		).Run()
		if err != nil {
			return packit.BuildResult{}, err
		}

		fmt.Println("Untaring dependency...")
		err = exec.Command("tar",
			"-zxf",
			filepath.Join(downloadDir, "php.tgz"),
			"--strip-components=1",
			"-C", phpLayer.Path,
		).Run()
		if err != nil {
			return packit.BuildResult{}, err
		}

		phpLayer.Build = true
		phpLayer.Launch = true
		phpLayer.Cache = true
		fmt.Printf("Layer path -> %s\n", phpLayer.Path)
		phpLayer.SharedEnv.Append("PATH", filepath.Join(phpLayer.Path, "bin"), string(os.PathListSeparator))
		phpLayer.SharedEnv.Append("PATH", filepath.Join(phpLayer.Path, "sbin"), string(os.PathListSeparator))

		matches, err := filepath.Glob(phpLayer.Path + "/lib/php/extensions/no-debug-non-zts-*")
		if err != nil {
			return packit.BuildResult{}, err
		}
		phpLayer.SharedEnv.Default("PHP_EXTENSION_DIR", matches[0])
		phpLayer.SharedEnv.Default("PHP_HOME", phpLayer.Path)
		fmt.Printf("%v \n", matches)
		// Add node version file
		err = ioutil.WriteFile(versionFile, []byte(version), 0644)
		if err != nil {
			return packit.BuildResult{}, err
		}

		return packit.BuildResult{
			Plan: context.Plan,
			Layers: []packit.Layer{
				phpLayer,
			},
		}, nil
	}
}
